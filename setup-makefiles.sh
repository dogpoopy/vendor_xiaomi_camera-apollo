#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=camera
DEVICE_COMMON=camera
VENDOR=xiaomi

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

ANDROID_ROOT="${MY_DIR}/../../.."

export TARGET_ENABLE_CHECKELF=true

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

function vendor_imports() {
    cat <<EOF >>"$1"
		"vendor/xiaomi/apollo",
		"hardware/qcom-caf/sm8250",
EOF
}

function lib_to_package_fixup_vendor_variants() {
    if [ "$2" != "vendor" ]; then
        return 1
    fi

    case "$1" in
        vendor.xiaomi.hardware.misys@1.0 | \
            vendor.xiaomi.hardware.misys@2.0 | \
            vendor.xiaomi.hardware.misys@3.0 | \
            vendor.xiaomi.hardware.misys@4.0)
            echo "${1}-vendor"
            ;;
        *)
            return 1
            ;;
    esac
}

function lib_to_package_fixup_system_variants() {
    if [ "$2" != "system" ]; then
        return 1
    fi

    case "$1" in
	vendor.xiaomi.hardware.campostproc@1.0 | \
	    vendor.xiaomi.hardware.misys@1.0 | \
	    vendor.xiaomi.hardware.misys@2.0 | \
	    vendor.xiaomi.hardware.misys@3.0 | \
	    vendor.xiaomi.hardware.misys@4.0)                                    
            echo "${1}-system"
            ;;
        *)
            return 1
            ;;
    esac
}

function lib_to_package_fixup_dependencies() {
    case "$1" in
        vendor.xiaomi.hardware.misys@1.0 | \
            vendor.xiaomi.hardware.misys@2.0 | \
            vendor.xiaomi.hardware.misys@3.0 | \
            vendor.xiaomi.hardware.misys@4.0) ;;
        *)
            return 1
            ;;
    esac
}

function lib_to_package_fixup() {
   lib_to_package_fixup_vendor_variants "$@"
   lib_to_package_fixup_system_variants "$@"
   lib_to_package_fixup_dependencies "$@"
}

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" true

# Warning headers and guards
write_headers "apollo"
sed -i 's|device/|vendor/|g' "$ANDROIDBP" "$ANDROIDMK" "$BOARDMK" "$PRODUCTMK"

write_makefiles "${MY_DIR}/proprietary-files.txt"

# Finish
write_footers
